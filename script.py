from src.example import myplot
import matplotlib.pyplot as plt

theta,r = myplot(4)

fig, ax = plt.subplots(
  subplot_kw = {'projection': 'polar'} 
)
ax.plot(theta, r)
ax.set_rticks([0.5, 1, 1.5, 2])
ax.grid(True)

fig.show()

