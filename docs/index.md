---
tags: #basics
title: Welcome to MkDocs Tutorial
---

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.



!!! info ":mega: Task 1" 

    ???+ example "This is an example"
    
        === "Tab 1"
            Markdown **content**.
        
            Multiple paragraphs.
        
        === "Tab 2"
            More Markdown **content**.
        
            - list item a
            - list item b
    
    ??? example
        
        << Test2 >>


<p markdown style="padding: 1rem; text-align:center; background-color:#66BB66">
![](images/catsinspace.jpg)
</p>


``` python
import numpy as np

def myfunction():
    pass
```

